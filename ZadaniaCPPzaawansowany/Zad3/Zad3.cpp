//Zadanie 3
//Napisz klas� StringConverter, kt�ra bedzie mia�a dwie metody(statyczne) :
//    std::string toCamelCase(std::string)
//    std::string toSnakeCase(std::string)
//    camelCase : snake_case
//    helloSDA : hello_s_d_a
//    getColour : get_colour
//    isThisFirstEntry : is_this_first_entry

#include <iostream>
#include <string>
#include "Zad3.hpp"
#include <algorithm>


std::string StringConverter::toCamelCase(std::string stringToCamelCase)
{
    for (int i = 0; i < stringToCamelCase.length(); i++)
    {
        if (stringToCamelCase[i] == '_')
        {
            stringToCamelCase.erase(i,1); //usuniecie '_'
            char toUpper = stringToCamelCase[i]; //na tej pozycji jest litera do zamiany na wielka
            toUpper -= 32;
            stringToCamelCase[i] = toUpper;
        }
    }
    return stringToCamelCase;
}

std::string StringConverter::toSnakeCase(std::string stringToSnakeCase)
{
    for (int i = 0; i < stringToSnakeCase.length(); i++)
    {
        if (stringToSnakeCase[i] >= 65 && stringToSnakeCase[i]<= 90)
        {
            char toLower = stringToSnakeCase[i]; 
            toLower += 32;
            stringToSnakeCase[i] = toLower;
            stringToSnakeCase.insert(i, 1, '_');
        }
    }
    return stringToSnakeCase;
}
