#pragma once
#include <iostream>
#include <string>

class StringConverter
{
public:
    static std::string toCamelCase(std::string stringToCamelCase);
    static std::string toSnakeCase(std::string stringToSnakeCase);
};
