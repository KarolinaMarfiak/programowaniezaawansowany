#include <iostream>
#include <string>
#include "Zad3.hpp"
#include <algorithm>

int main()
{
    std::string converted1 = StringConverter::toCamelCase("ala_ma_kota");
    std::cout << converted1 << std::endl;

    std::string converted2 = StringConverter::toSnakeCase("alaMaKota");
    std::cout << converted2;
}