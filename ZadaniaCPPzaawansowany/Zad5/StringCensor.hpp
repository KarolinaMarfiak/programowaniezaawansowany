#pragma once

#include <string>
#include <vector>

class StringCensor
{
private:
	std::vector<char> censoredLetters;
	bool shouldBeCensored( const char& c);

public:
	void addCensoredLetter(const char& c);
	std::string censor(std::string strToCensor);
};

