#include "StringCensor.hpp"

bool StringCensor::shouldBeCensored(const char& c)
{
	auto it = std::find(begin(censoredLetters), end(censoredLetters), c);
	return it != censoredLetters.end();
}

void StringCensor::addCensoredLetter(const char& c)
{
	censoredLetters.push_back(c);
}

std::string StringCensor::censor(std::string strToCensor)
{
	for (int i = 0; i < strToCensor.length(); ++i)
	{
		if (shouldBeCensored(strToCensor[i]))
		{
			strToCensor[i] = '*';
		}
	}
	return strToCensor;
}
