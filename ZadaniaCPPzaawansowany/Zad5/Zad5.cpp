//Zadanie 5
//Klasa: StringCensor
//Napisz klas� kt�ra b�dzie cenzurowa� podane stringi(zamienia� wymagane litery na gwiazk�), operacje kt�ra ma wspiera� ta klasa :
//-addCensoredLetter(char c)
//- std::string censor(std::string)

#include <iostream>
#include <string>
#include "StringCensor.hpp"

int main()
{
	StringCensor censorObj;

	censorObj.addCensoredLetter('k');
	censorObj.addCensoredLetter('u');

	std::string newCensoredWord = censorObj.censor("kukurydza");

	std::cout << newCensoredWord;
}

