#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

std::vector<char> compareTwoStrings(std::string str1, const std::string& str2)
{
    std::sort(str1.begin(), str1.end());
    auto itLast = std::unique(str1.begin(), str1.end()); 
    str1.erase(itLast, str1.end()); 

    std::vector<char> commonChars;

    std::for_each(str1.begin(), str1.end(), [&str2, &commonChars](char c)
        {
            if (str2.find(c) != std::string::npos)
            {
                commonChars.push_back(c);
            }

        });

    return commonChars;
}

std::ostream& operator<<(std::ostream& os, std::vector<char> vect)
{
    std::for_each(vect.begin(), vect.end(), [&os](char c)
        {
            os << c;
        });

    return os;
}

int main()

{
    std::string string1("banany");
    std::string string2("arbuzy");

    std::vector<char> commonChars = compareTwoStrings(string1, string2);
    std::cout << commonChars << std::endl;
}

