//Zadanie 6
//Stw�rz list� liczb od 1 do 1000. Rozdziel te liczby na dwie osobne listy : liczby pierwsze i nie liczby pierwsze.

#include <iostream>
#include <list>
#include <algorithm>

bool isPrimeNumber(const short int number)
{
    for (short int i = 2; i < number; ++i)
    {
        if (number % i == 0)
        {
            return false;
        }
    }
    return true;
}

int main()
{
    std::list<short int> allNumbers;
    std::list<short int> primeNumbers;
    std::list<short int> nonPrimeNumbers;


    for (short int i= 1; i <= 1000; ++i)
    {
        allNumbers.push_back(i);
    }

    std::cout << "Wszystkie liczby z listy: " << std::endl;
    std::for_each(allNumbers.begin(), allNumbers.end(), [](const short int& i) {std::cout << i << ' '; });

    std::for_each(allNumbers.begin(), allNumbers.end(), [&primeNumbers, &nonPrimeNumbers](const short int& i) {
        if (isPrimeNumber(i))
        {
            primeNumbers.push_back(i);
        }
        else
        {
            nonPrimeNumbers.push_back(i);
        }
        });

    std::cout << std::endl;

    std::cout << "Liczby pierwsze z listy to: " << std::endl;
    std::for_each(primeNumbers.begin(), primeNumbers.end(), [](const short int& i) {std::cout << i << ' '; });

    std::cout << std::endl;

    std::cout << "Liczby z listy nie bedace liczbami pierwszymi to: " << std::endl;
    std::for_each(nonPrimeNumbers.begin(), nonPrimeNumbers.end(), [](const short int& i) {std::cout << i << ' '; });

    
}

