//Zadanie  7
//Stworz list� 50 liczb ca�kowitych dodatnich i wype�nij j� losowymi warto�ciami.Wypisz j�.
//Nast�pnie posortuj i wypisz list� tak, by na pocz�tku znalaz�y si� warto�ci parzyste uporz�dkowane rosn�co, a za nimi warto�ci nieparzyste uporz�dkowane malej�co.
//Podpowiedz: Podziel list� na dwie osobne, odpowiednio posortuj, a nast�pnie ja po��cz w jedn�.

#include <iostream>
#include <list>
#include <algorithm>
#include <random>

bool isEvenNumber(int& number)
{
    if (number % 2 == 0)
    {
        return true;
    }
    return false;
}

int main()
{
    std::list<int> allNumbers;
    std::list<int> evenNumbers;
    std::list<int> oddNumbers;

    std::random_device rd;
    std::mt19937 g(rd());
    std::uniform_int_distribution<std::mt19937::result_type> dist1000(1, 1000);

    for (int i = 0; i < 50; ++i)
    {
        allNumbers.push_back(dist1000(g));
    }

    std::for_each(allNumbers.begin(), allNumbers.end(), [] (int& i) {std::cout << i << "  "; });

    std::for_each(allNumbers.begin(), allNumbers.end(), [&evenNumbers, &oddNumbers](int& i) {
        if (isEvenNumber(i))
        {
            evenNumbers.push_back(i);
        }
        else
        {
            oddNumbers.push_back(i);
        }
        });

    std::cout << std::endl;

    std::cout << "Liczby parzyste z listy to: " << std::endl;
    std::for_each(evenNumbers.begin(), evenNumbers.end(), [](int& i) {std::cout << i << ' '; });

    std::cout << std::endl;

    std::cout << "Liczby nieparzyste z listy to: " << std::endl;
    std::for_each(oddNumbers.begin(), oddNumbers.end(), [](int& i) {std::cout << i << ' '; });

    std::cout << std::endl;

    std::cout << "Liczby parzyste posortowane rosnaco: " << std::endl;
    evenNumbers.sort();
    std::for_each(evenNumbers.begin(), evenNumbers.end(), [](int& i) {std::cout << i << ' '; });
    std::cout << std::endl;

    std::cout << "Liczby nieparzyste posortowane malejaco: " << std::endl;
    oddNumbers.sort(std::greater<int>());
    std::for_each(oddNumbers.begin(), oddNumbers.end(), [](int& i) {std::cout << i << ' '; });


    std::list<int> mergedList;

    mergedList.splice(mergedList.end(), evenNumbers);
    mergedList.splice(mergedList.end(), oddNumbers);

    std::cout << std::endl;
    std::cout << "Polaczone dwie posortowane listy: " << std::endl;
    std::for_each(mergedList.begin(), mergedList.end(), [](int& i) {std::cout << i << ' '; });


}



