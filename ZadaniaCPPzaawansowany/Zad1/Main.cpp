#include <iostream>

// Zadanie 1. Napisz funkcj�, kt�ra odwr�ci podanego stringa.

std::string stringReverser(std::string str)
{
	std::reverse(str.begin(), str.end());
	
	return str;
}

int main()
{
	std::string str1("wspinaczka");
	std::cout << stringReverser(str1);
}