#include "Mors.hpp"

std::string MorseCodeTranslator::toMorseCode(std::string sentenceToTranslate)
{
    std::string translatedToMorse;

    for (int i = 0; i < sentenceToTranslate.length(); ++i)
    {
        if (sentenceToTranslate[i] >= 65 && sentenceToTranslate[i] <= 90)
        {
           translatedToMorse += charToMorse[sentenceToTranslate[i]];
           translatedToMorse += ' ';
        }
    }

    return translatedToMorse;
}

std::string MorseCodeTranslator::fromMorseCode(std::string morseCodeToTranslate)
{
    std::istringstream morseCodeToTranslateStream(morseCodeToTranslate); // zmiana na istringstream bo getline nie 'rozumie' std::string
    std::string translatedFromMorse;
    std::string tempMorseCode;

    while (std::getline(morseCodeToTranslateStream, tempMorseCode, ' '))
    {
        translatedFromMorse += morseToChar[tempMorseCode];
    }

    return translatedFromMorse;
}
