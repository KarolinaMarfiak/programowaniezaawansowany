//Zadanie 4
//Napisz klas�, kt�ra umo�liwi kodowanie i dekodowanie stringa na alfabet Morsea.Ignorujemy znaki specjalne
//KlasaMorseCodeTranslator
//toMorseCode
//fromMorseCode
//https ://morsecode.world/international/translator.html


#include <iostream>
#include "Mors.hpp"
#include <string>
#include <map>

int main()
{
    MorseCodeTranslator mors;

    std::string temp = mors.toMorseCode("ABBA");

    std::cout << temp << std::endl;
    std::cout << mors.fromMorseCode(temp);

}
