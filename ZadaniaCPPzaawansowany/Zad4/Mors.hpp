#pragma once
#include <string>
#include <map>
#include <sstream>


class MorseCodeTranslator
{
private:
	std::map<char, std::string> charToMorse =
	{
		{'A', ".-"},
		{'B', "-..."},
		{'C', "- . - ."},
		{'D', "- .."},
		{'E', "."},
		{'F', "..-."},
		{'G', "--."},
		{'H', "...."},
		{'I', ".."},
		{'J', ".---"},
		{'K', "-.-"},
		{'L', ".-.."},
		{'M', "--"},
		{'N', "-."},
		{'O', "---"},
		{'P', ".--."},
		{'Q', "--.-"},
		{'R', ".-."},
		{'S', "..."},
		{'T', "-"},
		{'U', "..-"},
		{'W', ".--"},
		{'X', "-..-"},
		{'Y', "-.--"},
		{'Z', "--.."},
	};

	std::map<std::string, char> morseToChar =
	{
		{".-", 'A' },
		{"-...",'B'},
		{"- . - .",'C'},
		{"- ..", 'D'},
		{".", 'E'},
		{"..-.", 'F'},
		{"--.", 'G'},
		{"....",'H'},
		{"..", 'I'},
		{".---",'J'},
		{"-.-",'K'},
		{".-..", 'L'},
		{"--",'M'},
		{"-.",'N'},
		{"---",'O'},
		{".--.",'P'},
		{"--.-",'Q'},
		{".-.", 'R'},
		{"...", 'S'},
		{"-", 'T'},
		{"..-", 'U'},
		{".--", 'W'},
		{"-..-", 'X'},
		{"-.--", 'Y'},
		{"--..", 'Z'},
	};

public:
	std::string toMorseCode(std::string sentenceToTranslate);
	std::string fromMorseCode(std::string morseCodeToTranslate);
};
